import React from 'react';
import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';
import { incrementar, decrementar, setear } from './reducers';
import UserForm from './components/UserForm';

class App extends React.Component{
  handleSubmit = payload => {
    console.log(payload);
  }
  render(){
      const { incrementar, decrementar, setear, valor } = this.props;

      const handleChange = e => {
          const {name, value} = e.target;
          this.setState({ [name]:value })
      }
      const handleSetear = () => {
        const { valor } = this.state;
          setear(Number(valor));
      }

      const handleSubmit = payload => {
        console.log(payload);
      }

      return(
        <div className="App">
          <header className="App-header" style={{paddingBottom: "50px"}}>
            <img src={logo} className="App-logo" alt="logo" />

            <h1>MANEJO DE ESTADO CON REDUX</h1>
            <p>VALOR: {valor}</p>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <button onClick={incrementar}>Incrementar</button>
              <button onClick={decrementar}>Decrementar</button>
              <input name='valor' onChange={handleChange}/>
              <button onClick={handleSetear}>Setear</button>
            </div>

            <h1>FORMULARIO CON REDUX</h1>        
            <UserForm  onSubmit={this.handleSubmit} />

          </header>
        </div>
      );
  }
}

// esto es lo mismo que arriba
// function App(props) {
//   const { incrementar, decrementar, setear, state } = props;

//   const handleChange = e => {
//     const {name, value} = e.target;
//     // this.setState({ [name]:value }) --> esto no funciona acá, por eso coloque la linea de abajo y funciona parecido al ejemplo
//     setear(value);
//   }

//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />

//         <p>VALOR: {state}</p>
//         <button onClick={incrementar}>Incrementar</button>
//         <button onClick={decrementar}>Decrementar</button>

//         <input name='state' onChange={handleChange}/>
//       </header>
//     </div>
//   );
// }

const mapStateToProps = state => {
  return {
    valor: state.contador,
  }
}

const mapDispatchToProps = dispatch => ({
  incrementar: () => dispatch(incrementar()),
  decrementar: () => dispatch(decrementar()),
  setear: payload => dispatch(setear(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
