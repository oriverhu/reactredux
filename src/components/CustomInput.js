import React, {Component} from 'react';

export default class CustomInput extends Component{
    render(){
        const { input, meta, title, ...props } = this.props;

        return(
            <div>
               {title && <div><span>{title}</span></div> }
                <input {...input} { ...props } /> 
                { meta.submitFailed && meta.error && <small>{meta.error}</small>}
            </div>
        )
    }
}