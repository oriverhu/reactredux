import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import CustomInput from './CustomInput';

const validate = values => {
    const errors = {}
    if(!values.name){
        errors.name = 'Nombre obligatorio';
    }
    if(!values.lastname){
        errors.lastname = 'Apellido obligatorio';
    }
    return errors;
}

class UserForm extends Component{
    render(){
        const { handleSubmit } = this.props;

        return(            
            <form onSubmit={handleSubmit}>
                 <Field component={CustomInput} type="text" name="name"  placeholder="Nombre" title="Nombre" />
                 <Field component={CustomInput} type="text" name="lastname"  placeholder="Apellido" title="Apellido"  />
                {/* <Field component="input" type="text" name="name"  placeholder="Nombre" />
                <Field component="input" type="text" name="lastname" placeholder="Apellido" /> */}
                <button type="submit">Enviar</button>
            </form>
        )
    }
}

export default reduxForm({
    form: 'user', //debe ser unico con respecto a los otros forms
    validate
})(UserForm)