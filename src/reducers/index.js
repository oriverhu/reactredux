const INCREMENTAR = 'CONTADOR/INCREMENTAR';
const DECREMENTAR = 'CONTADOR/DECREMENTAR';
const SETEAR = 'CONTADOR/SETEAR';

export const incrementar = () => ({
    type: INCREMENTAR
})
export const decrementar = () => ({
    type: DECREMENTAR
})
export const setear = payload => ({
    type: SETEAR,
    payload
})

const initialState = 0;
//reducer tienen que retornar estados inmutables
export default function(state = initialState, action){
    switch (action.type) {
        case INCREMENTAR:
                return state+1;
            break;
        case DECREMENTAR:
                return state-1;
            break;
        case SETEAR:
                return action.payload;
            break;    
        default:
            return state
            break;
    }  
}